
import React, { useState, useEffect } from 'react';
import { Provider } from 'react-redux';
import store from './store';

import './App.css';
import ProductForm from './products/ProductForm';
import ProductsList from './products/ProductsList';


interface AppProps {}

const App: React.FC<AppProps> = ({  }) => {
 

  return (
    <Provider store={store}>
      <div className="App">
        <ProductsList ></ProductsList>
        <ProductForm ></ProductForm>
      </div>
    </Provider>
    
    
  );
};

export default App;
