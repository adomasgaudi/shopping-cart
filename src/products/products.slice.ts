import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";



export interface Product {
  title: string;
  price: number;
  id: string;
}

const initialState: Product[] = [
  { title: "escape from tarkov", price: 60, id: 'eft'},
  { title: "hunt showdown", price: 40, id: 'hs'},
  { title: "hell let loose", price: 35, id: 'hll'},
]



const productSlice = createSlice({
  name: 'productz',
  initialState,
  reducers: {
    addProduct: (state, action: PayloadAction<Product>) => {
      return [action.payload,...state]
    }
  }

})


export const {addProduct} = productSlice.actions;

export const getProductsSelector = (state: RootState) => state.productz


export default productSlice.reducer;