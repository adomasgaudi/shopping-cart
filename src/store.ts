import { configureStore } from "@reduxjs/toolkit";
import productz from "./products/products.slice";



const store = configureStore({
  reducer: { productz }
})


export type RootState = ReturnType<typeof store.getState>

export default store 